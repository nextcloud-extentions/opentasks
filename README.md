# OpenTasks++

__An Open Source Task App for Android__

Fork of the OpenTasks application.

## Requirements

* Android SDK Level 23 (Android 6+)

## Download

Support forum:
http://mbagc.ru/forum/viewtopic.php?f=39&t=624

## Original application

Original repository:
https://github.com/dmfs/opentasks

Website of the original program:
https://opentasks.app


## License

Copyright (c) 2022 AGC
Copyright (c) 2013-2015 Marten Gajda

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


